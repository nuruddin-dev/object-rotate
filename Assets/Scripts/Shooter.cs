using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    private Vector3 initialShooterPosition;
    private bool isMouseEnter, isMouseClicked;
    public float rotationSpeed = 25.0f;
    
    void Start()
    {
        initialShooterPosition = transform.position;
    }

    
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            isMouseClicked = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            isMouseClicked = false;
            isMouseEnter = false;
        }

        if (isMouseClicked && !isMouseEnter)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            // Mouse postion deviation from the Shooter initial position.
            Vector3 mousePositionDeviation = mousePosition - initialShooterPosition;
            float mouseAngle = Mathf.Atan2(mousePositionDeviation.y, mousePositionDeviation.x) * Mathf.Rad2Deg; // The angle is calculated from the right Y-axis (like normal co-ordinate calculation).
            // Now we need the angle difference from the top X-axis. 
            mouseAngle = mouseAngle - 90f; // Now for right side, angle will be negative and for left side, angle will be positive.
            float mouseAngleDeviation = Mathf.Abs(mouseAngle);

            float shooterRotation = transform.rotation.z; // Negative means on right side and positive means on left side.
            float shooterAngleDeviation = transform.eulerAngles.z; // This angle will start from the current position and will increase to the left direction.
            if (shooterAngleDeviation > 90f) // When the shooter angle is between (270-360) degree we 
            {
                shooterAngleDeviation = 360 - shooterAngleDeviation;
            }
            
            if (shooterAngleDeviation == 0)
            {
                // If mouse is on right side.
                if (mouseAngle < 0) GoRight();
                // If mouse is on left side
                if (mouseAngle > 0) GoLeft();
            }
            
            // If mouse is on right side.
            if (mouseAngle < 0)
            {
                // If Shooter is on right side
                if (shooterRotation < 0)
                {
                    Debug.Log("mouse -> right, shooter -> right");
                    if (mouseAngleDeviation > shooterAngleDeviation) GoRight();
                    if (mouseAngleDeviation < shooterAngleDeviation) GoLeft();
                }
                // If Shooter is on left side
                if (shooterRotation > 0)
                {
                    Debug.Log("mouse -> right, shooter -> left");
                    GoRight();
                }
            }
            // If mouse is on left side
            if (mouseAngle > 0)
            {
                // If Shooter is on left side
                if (shooterRotation > 0)
                {
                    Debug.Log("mouse -> left, shooter -> left");
                    if (mouseAngleDeviation > shooterAngleDeviation) GoLeft();
                    if (mouseAngleDeviation < shooterAngleDeviation) GoRight();
                }
                // If Shooter is on right side
                if (shooterRotation < 0)
                {
                    Debug.Log("mouse -> left, shooter -> right");
                    GoLeft();
                }
            }
        }
    }

    void GoRight()
    {
        // It will rotate but not smoothly.
        // transform.Rotate(Vector3.back, 10f);
        
        //----------Smooth Rotation-----------
        Quaternion targetRotation = Quaternion.Euler(0, 0, transform.eulerAngles.z - rotationSpeed);
        // Using Quaternion.Slerp to smoothly interpolate between the current rotation and the target rotation
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }

    void GoLeft()
    {
        // It will rotate but not smoothly.
        // transform.Rotate(Vector3.forward, 10f);
        
        //----------Smooth Rotation-----------
        Quaternion targetRotation = Quaternion.Euler(0, 0, transform.eulerAngles.z + rotationSpeed);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
    
    void OnMouseOver()
    {
        if(isMouseClicked)
            isMouseEnter = true;
    }

    void OnMouseExit()
    {
        isMouseEnter = false;
    }
    
}
